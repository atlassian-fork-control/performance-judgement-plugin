def buildLog = new File(basedir, "build.log")

def found = [
        "[WARNING] The volume of baseline jira.dashboard is 104, which is under the minimum threshold of 300"   : false,
        "[WARNING] The volume of experiment jira.agile.work is 153, which is under the minimum threshold of 300": false,
        "[WARNING] The volume of experiment nonexistent is 0, which is under the minimum threshold of 300": false,
]

buildLog.eachLine { line ->
    found.entrySet()
            .stream()
            .filter({ !it.value })
            .filter({ line.contains(it.key) })
            .forEach { found.put(it.key, true) }
}

found.entrySet()
        .stream()
        .filter({ !it.value })
        .peek { println "[ERROR] Regression '${it.key}' not found in $buildLog" }
        .findAny()
        .ifPresent { throw new RuntimeException("An expected regression was not found in $buildLog") }

