def regressionFound = false

def buildLog = new File(basedir, "build.log")
buildLog.eachLine { line ->
    if (line.contains(
            "[WARNING] The 'experiment' location PT1.744S is a regression against 'baseline' location PT1.645S for " +
                    "Interaction(key=jira.dashboard, toleranceRatio=0.01, label=View Dashboard)"
    )) {
        regressionFound = true
    }
}

if (!regressionFound) {
    throw new RuntimeException("The expected regression was not found in " + buildLog)
}