package com.atlassian.performance.judgement

/**
 * Required by reflection-based Maven plugin framework, which requires a nullary constructor.
 */
data class TolerableInteraction(
        var interaction: String? = null,
        var toleranceRatio: Float = 0.05f,
        var label: String? = null
) {
    fun becomeProper(): Interaction {
        return Interaction(
                key = interaction!!,
                toleranceRatio = toleranceRatio,
                label = label ?: interaction!!
        )
    }
}