package com.atlassian.performance.judgement.analytics.log

import com.atlassian.performance.judgement.DurationData
import com.atlassian.performance.judgement.StatsReader
import org.apache.maven.plugin.logging.Log
import java.io.File
import java.io.StringReader
import java.nio.file.Files
import javax.json.Json

internal data class AnalyticLogsReader(
        private val directory: File,
        private val log: Log
) : StatsReader {

    override fun read(
            interactions: Iterable<String>
    ): Map<String, DurationData> {
        if (directory.canRead()) {
            return readDirectory(interactions)
        } else {
            log.info("Skipped analytics logs analysis, because $directory is not readable")
            return emptyMap()
        }
    }

    private fun readDirectory(interactions: Iterable<String>): MutableMap<String, DurationData> {
        val interactionData = mutableMapOf<String, DurationData>()
        val metric = "readyForUser"
        val logs = directory.listFiles() ?: throw RuntimeException("Listing files from $directory fails")
        logs.toList()
                .asSequence()
                .flatMap({ Files.lines(it.toPath()).iterator().asSequence() })
                .filter { it.contains("browser.metrics.navigation") }
                .map { it.split('|')[ANALYTICS_PAYLOAD_INDEX] }
                .map { Json.createReader(StringReader(it)).readObject() }
                .map { InteractionDuration(it.getString("key"), it.getString(metric).toDouble()) }
                .filter { interactions.contains(it.interaction) }
                .sorted()
                .forEach {
                    val durationData = interactionData.getOrPut(
                            it.interaction,
                            { DurationData.createEmptyMilliseconds() }
                    )
                    durationData.stats.addValue(it.milliDuration)
                }
        return interactionData
    }

    private data class InteractionDuration(
            val interaction: String,
            val milliDuration: Double
    ) : Comparable<InteractionDuration> {

        override fun compareTo(other: InteractionDuration): Int {
            return milliDuration.compareTo(other.milliDuration)
        }
    }

    companion object {
        private val ANALYTICS_PAYLOAD_INDEX = 8
    }
}
