package com.atlassian.performance.judgement.soke

import net.lingala.zip4j.core.ZipFile
import java.io.File
import java.nio.file.Files

internal class SokeLogExtractor {

    fun extract(
            reportZip: File
    ): File {
        val destination = Files.createTempDirectory(reportZip.name).toFile()
        val logFileName = "test-0-result.log"
        ZipFile(reportZip).extractFile(logFileName, destination.absolutePath)
        return File(destination, logFileName)
    }
}
