package com.atlassian.performance.judgement.soke

import com.atlassian.performance.judgement.DurationData
import com.atlassian.performance.judgement.StatsReader
import com.atlassian.performance.judgement.soke.SokeLogReader.SokeLogField.INTERACTION
import com.atlassian.performance.judgement.soke.SokeLogReader.SokeLogField.NANO_DURATION
import org.apache.maven.plugin.logging.Log
import java.io.File
import java.nio.file.Files

internal data class SokeLogReader(
        private val report: File,
        private val extractor: SokeLogExtractor,
        private val log: Log
) : StatsReader {

    internal enum class SokeLogField(val index: Int) {
        INTERACTION(3),
        NANO_DURATION(5)
    }

    override fun read(
            interactions: Iterable<String>
    ): Map<String, DurationData> {
        if (report.canRead()) {
            return readReport(interactions)
        } else {
            log.info("Skipped soke report analysis, because $report is not readable")
            return emptyMap()
        }
    }

    private fun readReport(interactions: Iterable<String>): MutableMap<String, DurationData> {
        val interactionData = mutableMapOf<String, DurationData>()
        val logFieldSplitter = "\\|\\|".toRegex()
        val log = extractor.extract(report).toPath()
        Files.lines(log)
                .map { it.split(logFieldSplitter) }
                .filter { interactions.contains(it[INTERACTION.index]) }
                .map { InteractionDuration(it[INTERACTION.index], it[NANO_DURATION.index].toDouble()) }
                .sorted()
                .forEach {
                    val durationData = interactionData.getOrPut(
                            it.interaction,
                            { DurationData.createEmptyNanoseconds() }
                    )
                    durationData.stats.addValue(it.nanoDuration)
                }
        return interactionData
    }

    private data class InteractionDuration(
            val interaction: String,
            val nanoDuration: Double
    ) : Comparable<InteractionDuration> {

        override fun compareTo(other: InteractionDuration): Int {
            return nanoDuration.compareTo(other.nanoDuration)
        }
    }
}
