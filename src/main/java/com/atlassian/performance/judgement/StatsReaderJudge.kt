package com.atlassian.performance.judgement

internal data class StatsReaderJudge(
        private val perInteractionJudge: PerInteractionJudge,
        private val volumeJudge: VolumeJudge,
        private val dataReporter: DataReporter
) {

    fun judge(
            criteria: PerformanceCriteria,
            baselineMeter: StatsMeter,
            experimentMeter: StatsMeter
    ): Boolean {
        val baselineData = baselineMeter.measureLocations(criteria)
        val experimentData = experimentMeter.measureLocations(criteria)
        dataReporter.report(
                criteria,
                listOf(baselineData, experimentData)
        )
        return volumeJudge.judge(baselineData, criteria)
                .and(volumeJudge.judge(experimentData, criteria))
                .and(perInteractionJudge.judge(criteria, baselineData, experimentData))
    }
}
