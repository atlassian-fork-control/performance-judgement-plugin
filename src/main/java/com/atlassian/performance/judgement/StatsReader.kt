package com.atlassian.performance.judgement

internal interface StatsReader {

    fun read(
            interactions: Iterable<String>
    ): Map<String, DurationData>
}
